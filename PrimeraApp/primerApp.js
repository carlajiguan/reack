import React from 'react'

// Paso de props 
const primerApp = ({saludos, titulo}) => {
    return (
        <div>
            <h1>{saludos}</h1>
            <h3>{titulo}</h3>
        </div>
    )
}

primerApp.prototype = {
    saludos: PropTypes.string.isRequired
}

primerApp.defaultProps = {
    titulo: "Soy un titulo"
}

export default primerApp

// En el otro archivo enviar el componente

{/* <primerApp  /> */}
