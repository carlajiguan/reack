import React from 'react'
import PropTypes from 'prop-types'

const ConterApp = ({value}) => {
    return (
        <div>
            <h1>CounterAPP</h1>
            <div>{value}</div>

            <button onClick={ (e)=>{console.log(e)} }>+1 </button>
        </div>
    )
}

ConterApp.propTypes = {
    value: PropTypes.number

}

export default ConterApp
