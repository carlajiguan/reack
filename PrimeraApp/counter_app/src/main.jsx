import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import ConterApp from './component/ConterApp'
import PropTypes from 'prop-types'



ReactDOM.render(
  <React.StrictMode>
    <ConterApp value={20}/>
  </React.StrictMode>,
  document.getElementById('root')
)
