'use strict';
const mysql = require('mysql');

const conexion = mysql.createConnection({
  host:"localhost",
  user: "root",
  password: "12345678",
  database: "prueba"
});

function main() {
   
  const {BigQuery} = require('@google-cloud/bigquery');

 //Donde esta el json de conexion 
  const options = {
    keyFilename: 'C:/Users/Carla Jiguan/Documents/BigQueryDemo/plated-mantis-32981.json',
    projectId: 'plated-mantis-329813',
  };

  const bigquery = new BigQuery(options);
  
  //Conversor de fecha linux
    function convertEvent_Timestamp(val){
    var timestamp = val;
    var d = new Date(timestamp/1000);
    let conversion=   d.getFullYear() + "-" + 
                        ("00" + (d.getMonth() + 1)).slice(-2) + "-" + 
                        ("00" + d.getDate()).slice(-2) + " " + 
                        ("00" + d.getHours()).slice(-2) + ":" + 
                        ("00" + d.getMinutes()).slice(-2) + ":" + 
                        ("00" + d.getSeconds()).slice(-2)
        return conversion;
    }



     class model  {
        user_id= 0;
        event_date= new Date();
        event_timestamp = new Date();
        event_name= "";
        device = new Object();
        geo = new Object();

     constructor(user_id, event_date, event_timestamp, event_name, device, geo){
         this.user_id = user_id;
         this.event_date = event_date;
         this.event_timestamp = event_timestamp
         this.event_name = event_name;
         this.device= device;
         this.geo= geo;
     }
     

     
    };


  let arreglo = [];

  
  // Para eliminar los datos que no quieren :V
  function deleteSpecificKeys(obj, keys){
    const instancia = new model();
    for(let i=0; i< keys.length; i++){

     // console.log( "Esto trae keys  "+i +obj);
     
      

      if(keys[i] == 'event_timestamp'){
          let  convert =convertEvent_Timestamp(obj[keys[i]])
          obj[keys[i]] = convert;
          instancia.event_timestamp = convert;
        
        
      }else if(keys[i] == 'user_id'){
          obj[keys[i]] = obj[keys[i]];
          instancia.user_id = obj[keys[i]];
         
      }
      else if(keys[i] =="event_date")
      {
          obj[keys[i]] = obj[keys[i]];
          instancia.event_date = obj[keys[i]];
          
      }
      else if(keys[i] =="event_name")
      {
          obj[keys[i]] = obj[keys[i]];
          instancia.event_name = obj[keys[i]];
         
      }
      else if(keys[i] =="device")
      {
          obj[keys[i]] = obj[keys[i]];
         instancia.device =obj[keys[i]];
        
         
      }
      else if (keys[i] =="geo")
      {
          obj[keys[i]] = obj[keys[i]];
          instancia.geo = obj[keys[i]];
            
        }
        else{
            delete obj[keys[i]];
        }

        //console.log(instancia);



      
    }


    //console.log(obj);
    //arreglo.push(obj);
    
    return instancia;

}


 // conexion.end();
//});
  let salida= [];

async function query() {
    
    const query =  ` SELECT * FROM \`plated-mantis-329813.analytics_290798038.events_intraday_20211025\` LIMIT 1000`;
    const options = {
      query: query
    //  location: 'US',
    };

    const [job] = await bigquery.createQueryJob(options);
    const [rows] = await job.getQueryResults();
    conexion.connect();
    rows.forEach(row => {
        let data = row;
        let map = JSON.stringify(data);
        let o = JSON.parse(map);
        
        let toDelete = ["user_id", "event_date", "event_timestamp","event_name", "device", "geo" ,"event_params" ,"event_previous_timestamp","event_value_in_usd","event_bundle_sequence_id","event_server_timestamp_offset","user_pseudo_id",
        "privacy_info","user_properties" ,"items","ecommerce","event_dimensions" ,"platform" ,"stream_id" ,"traffic_source" ,"app_info" 
        ,"user_ltv" ,"user_first_touch_timestamp"];
        
        salida = deleteSpecificKeys(o, toDelete);
      
       
    let cast = JSON.stringify(salida);
    let out= { user_id: salida.user_id, detalle:cast };
 
    conexion.query("INSERT into data_json SET ?", out, function(err, result, fields) {
 
      if(err) throw err;

      if(result){
        
      }
      //console.log(result);
    });
    
 
  });
  conexion.end();
}
  
  query();
  
 
}

main(...process.argv.slice(2));

