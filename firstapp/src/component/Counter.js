import React, {useState} from 'react';
import PropTypes from 'prop-types';
import '../App.css';

const Counter = ({value}) => {

const [counter, setCounter] = useState(value);


    const handleAdd = ()=> {
        setCounter(counter+1); //forma 1
       // setCounter( ()=> {});
    }

    
    const handleSubs = ()=> {
        setCounter(counter-1); 
    }

    const handleReset = ()=> {
        setCounter(value); 
    }

    return (
        <div className="div-position">
            <h1>Counter +</h1>
            <h3>{counter}</h3>
            <button className="" onClick={handleAdd}>+1</button>
            <button className="" onClick={handleReset}>Reset</button>
            <button className="" onClick={handleSubs}>-1</button>
            
        </div>
    )
}

Counter.propTypes = {
    value: PropTypes.number
}

export default Counter
