import { types } from "../types/types";

//Reducer recibe un state y un reducer 

// Los estados siempre deben regresar algo es decir deben estar inicializados como abajo
// y no dejarlos   asi ....  export const authReducer = ( state , action ) =>{ porque regresan undefined, o null 
export const authReducer = ( state = {}, action ) =>{

    switch (action.type) {
        // Para loguearse
        case types.login:
            return {
                uid: action.payload.uid,
                name: action.payload.displayName
            }

        // Para cerrar sesion
        case types.logout:
            return { }
    
        default:
           
            return state;
    }
}