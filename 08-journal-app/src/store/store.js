
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { authReducer } from '../reducers/authReducer';

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;


const reducers = combineReducers({
    auth: authReducer
});

//Los store  solo aceptan un reducer, por lo cual
// se utiliza los combineReducers para contener varios reducers y estos se envian como uno solo :P
export const store = createStore(
    reducers,
    // Este es un middleware pero no el que se usa para trabajar las tareas asincronas
    // por lo tanto se usarara el que esta copiando en la line a *5*
    //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

    composeEnhancers(
        applyMiddleware(thunk)
    )


    );
