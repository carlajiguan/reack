// funciones o accioens que manejaran las auth 
import { firebase, googleAuthProvider } from '../firebase/firebase-config';
import { types } from '../types/types';


/*{
    return {
        type:types.login,
        payload: {
            uid,
            displayName
        }
    }
}*/
// accion asincrona, recibmos email y pass
export const startLoginEmailPassword = (email, password) =>{
    return (dispatch) => {
        setTimeout(() =>{
            dispatch (login(123, 'pedro'));

        }, 3500)
    }

}

// Esta accion la llamamos en el login screen
export const startGoogleLogin = () => {
      return ( dispatch )=> {
            firebase.auth().signInWithPopup(googleAuthProvider)
            .then(({ user }) => {
                dispatch(
                    login( user.uid, user.displayName)
                )
            }); 
      }
}


export const login=(uid, displayName) => ({
    type:types.login,
    payload: {
        uid,
        displayName
    }
})
 