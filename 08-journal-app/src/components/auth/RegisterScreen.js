import React from 'react'
import {Link} from 'react-router-dom'
import { useForm } from '../../hooks/useForm'


export const RegisterScreen = () => {

    /**
     * {
     *  name: 'Nombre',
     *  email: 'email',
     *  password: 'pasdfsd',
     *  password2: '12345678',
     * }
     * 
     * 
     * 
     */
//  Custom hook que realizamos
    const [] = useForm({
        name: 'Nombre',
        email: 'email',
        password: 'pasdfsd',
        password2: '12345678',
    })

    const handleRegister = (e) => {
       // console.log(name, email, password, password2);
    }


    return (
        <>
        <h3 className="auth__title">Register</h3>
        <form>
        <input 
                type="text"
                placeholder="Name"
                name= "name"
                className="auth__input"
                autoComplete="off"
            />
            <input 
                type="text"
                placeholder="Email"
                name= "email"
                className="auth__input"
                autoComplete="off"
            />
            <input 
                type="password"
                placeholder="Password"
                name= "password"
                className="auth__input"

            />
             <input 
                type="password"
                placeholder="Confirm password"
                name= "confirm"
                className="auth__input"

            />
             <button
                type="submit"
                className="btn btn-primary btn-block mb-1"
               
                >Register
                </button>

                <Link 
                className="link"
                to="/auth/login"
                >
                    Already register?
                </Link>
        </form>
    </>
       
    )
}
