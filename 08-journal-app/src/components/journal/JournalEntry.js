import React from 'react'

export const JournalEntry = () => {
    return (
        <div className="journal__entry pointer">
            <div className="journal__entry-picture"
            style={{
                backgroundSize: 'cover',
                backgroundImage: 'url(https://w7.pngwing.com/pngs/375/617/png-transparent-nike-logo-check-mark-green-tick-angle-leaf-text.png)'
            }}
            >
            </div>
            <div className="journal__entry-body">
                <p className="journal__entry-title">
                    Un nuevo dia
                </p>
                <p className="journal__entry-content">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p>

            </div>
            <div className="journal__entry-date-box">
                <span>Thursday</span>
                <h4>21</h4>

            </div>
            
        </div>
    )
}
