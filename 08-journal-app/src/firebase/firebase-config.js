// import de firebase para version 9
import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import 'firebase/compat/auth';

// config de firebase
const firebaseConfig = {
  apiKey: "AIzaSyCBKq8_R33zo5cwrofDmZbgwIAAsNOz-6Q",
  authDomain: "react-journal-login.firebaseapp.com",
  projectId: "react-journal-login",
  storageBucket: "react-journal-login.appspot.com",
  messagingSenderId: "1097302860739",
  appId: "1:1097302860739:web:6f7198ab1912dbf9ab9376"
};

//const app = initializeApp(firebaseConfig); 
firebase.initializeApp(firebaseConfig); //la DB

const db = firebase.firestore(); // referencia a firestore 
//referiencia de firebae a firestore
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();


export {
    db,
    googleAuthProvider,
    firebase
}