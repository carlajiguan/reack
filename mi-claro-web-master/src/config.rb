# Require any additional compass plugins here.
require 'fileutils'

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "/resources/css"
sass_dir = "sass"

#output_style = :expanded
#output_style = :nested
#output_style = :compressed
# or :nested or :compact or :compressed
Encoding.default_external = 'UTF-8'
#output_style = (environment == :production) ? :compressed : :expanded
output_style = :compressed

#environment = :production
environment = :production

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

color_output = false

preferred_syntax = :sass

# Callbacks
on_stylesheet_saved do |file|
    $filename = "styles.css"
    if File.exists?(file) && File.basename(file) == $filename
        FileUtils.cp(file, File.dirname(file) + "/../../../dist/assets/css/" + $filename)
    end
end