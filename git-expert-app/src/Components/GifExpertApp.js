import React, {useState} from 'react'
import { AddCategory } from './AddCategory';

const GifExpertApp = () => {

   // const categorias = ['One Punch', 'Samurai X', 'Dragon Ball'];
    const [categories, setCategories] = useState(['One Punch', 'Samurai X', 'Dragon Ball']);

    const handleAdd =() =>{
        // Forma 1
      //  setCategories([...categories, 'HunterXHunter']);
        // El callback es el primer argumento es el valor del estado anterior 
        //Formas 2  recibe un callbacks mas la nueva categoria
        setCategories(cats => [...cats, 'HunterXHunter']);
        console.log(categories);
    }
    console.log(categories);
    return (
        <div>
            <h1>GifExpertApp</h1>
            <AddCategory/>
            <hr/>
            <button onClick={handleAdd}>Agregar</button>
           <ol>
               {
                   categories.map(value => {
                    return <li key={value} >{value}</li>
                   })
               }
           </ol>
        </div>
    )
}

export default GifExpertApp
