import React, {useState} from 'react'

export const AddCategory = () => {

    const [inputValue, setInputValue] = useState("Hola Mundo")
    const handleInputChange =(e)=> {
        console.log(e.target.value);
        setInputValue(e.target.value);
    }

    const  handleSubmit = (e)=> {

    }

    return (
        <form>
           
            <input
            type="text"
            value={inputValue}
            onChange={handleInputChange}
            />
        </form>
    )
}
